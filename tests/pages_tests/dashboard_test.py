
from app_main import app
from tests import OhmTestCase


class DashboardTest(OhmTestCase):
    def test_get(self):
        with app.test_client() as c:
            response = c.get('/dashboard')
            assert "Ready to begin assessment" in response.data
            # dropdown has Community link
            assert 'Community' in response.data
            # check logged in as user 1 and points are displayed correctly
            # also checks that DB migration is performed correctly
            assert '<span>1000</span>' in response.data
