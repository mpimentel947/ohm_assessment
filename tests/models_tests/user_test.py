from tests import OhmTestCase


class UserTest(OhmTestCase):
    def test_get_attribute(self):
        assert self.elvis.get_attribute("LOCATION") == "USA"
        assert self.chuck.get_attribute("LOCATION") == "EUROPE"

    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []

    def test_add_and_del_multi(self):
        self.justin.add_multi("PHONE", '+14086441235')
        self.chuck.add_multi("PHONE", '+14086445679')
        assert self.justin.get_multi("PHONE") == ['+14086441235']
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678', '+14086445679']
        self.chuck.delete_multi("PHONE", '+14086445679')
        self.justin.delete_multi("PHONE", '+14086441235')
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []

    def test_replace_multi(self):
        self.justin.replace_multi("PHONE", ['+14086441232','+14086441244'])
        self.chuck.replace_multi("PHONE", ['+14086441233'])
        assert self.justin.get_multi("PHONE") == ['+14086441232','+14086441244']
        assert self.chuck.get_multi("PHONE") == ['+14086441233']
        self.justin.delete_multi("PHONE", '+14086441232')
        self.justin.delete_multi("PHONE", '+14086441244')
        self.chuck.replace_multi("PHONE", ['+14086441234','+14086445678'])
        assert self.justin.get_multi("PHONE") == []
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
