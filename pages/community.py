from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User

from sqlalchemy import create_engine

@app.route('/community', methods=['GET'])
def community():

    login_user(User.query.get(1))

    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    res = engine.execute('SELECT user_id, display_name, tier, point_balance FROM user ORDER BY signup_date DESC LIMIT 5;').fetchall()

    users = []

    for r in res:
        uid,uname,tier,point = r
        location = engine.execute('SELECT attribute from rel_user WHERE user_id=' + str(uid) + ' and rel_lookup="LOCATION"').first()
        if location:
            location = location[0]
        user = [uid,uname,tier,point,location]
        numbers = []
        numbers_res = engine.execute('SELECT attribute FROM rel_user_multi WHERE user_id=' + str(uid) + ' and rel_lookup="PHONE"').fetchall()
        for nr in numbers_res:
            numbers.append(nr[0])
        user.append(numbers)
        users.append(user)

    args = {
            'gift_card_eligible': True,
            'cashout_ok': True,
            'user_below_silver': current_user.is_below_tier('Silver'),
            'comm': users,
    }

    return render_template("community.html", **args)

